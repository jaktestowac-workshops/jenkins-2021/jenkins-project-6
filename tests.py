import os
import unittest
from selenium import webdriver

class ShopGdprTests(unittest.TestCase):
  def setUp(self):
      if os.name == 'nt':
          self.driver = webdriver.Chrome(executable_path=r"libs\chromedriver.exe")
      else:
          chrome_options = webdriver.ChromeOptions()
          chrome_options.add_argument('--no-sandbox')
          chrome_options.add_argument('--headless')
          chrome_options.add_argument('--disable-gpu')
          self.driver = webdriver.Chrome(options=chrome_options)
      self.base_url = 'https://autodemo.testoneo.com/en/'

  def tearDown(self):
      print('tearDown')
      self.driver.quit()

  def test_contact_us_page_title(self):
      contact_us_page = 'https://autodemo.testoneo.com/en/contact-us'
      self.driver.get(contact_us_page)
      expected_title = 'Contact us'
      actual_title = self.driver.title
      print('actual_title:', actual_title)
      self.assertEqual(expected_title, actual_title)
